use regex::Regex;

lazy_static! {
    static ref RE: Regex = Regex::new(r"^https?://(.+?)/(.+)/pipelines/(.+)$").unwrap();
}

#[derive(Debug, PartialEq)]
pub struct Pipeline {
    pub web_url: String,

    pipeline_id: String,
    project_id: String,
    repo_url: String,
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Job {
    pub name: String,
    pub status: String,

    id: u32,
    stage: String,
}

impl Pipeline {
    pub fn from_url(url: &str) -> Pipeline {
        let caps = RE.captures(url).unwrap();

        Pipeline {
            repo_url: caps.get(1).expect("Invalid pipeline").as_str().to_string(),

            project_id: caps
                .get(2)
                .expect("Invalid domain")
                .as_str()
                .replace("/", "%2F")
                .to_string(),

            pipeline_id: caps.get(3).expect("Invalid project").as_str().to_string(),

            web_url: url.to_string(),
        }
    }

    // @Matt TODO: howto test?
    pub fn get_jobs(&self, token: &str) -> Result<Vec<Job>, reqwest::Error> {
        let req_url = format!(
            "https://{repo_url}/api/v4/projects/{project_id}/pipelines/{pipeline_id}/jobs",
            repo_url = self.repo_url,
            project_id = self.project_id,
            pipeline_id = self.pipeline_id
        );

        let mut res = reqwest::Client::new()
            .get(&req_url)
            .header("PRIVATE-TOKEN", token)
            .send()?;

        if !res.status().is_success() {
            println!("{}", res.text()?);
            panic!("Response Failed!");
        }

        let jobs: Vec<Job> = match res.json() {
            Ok(j) => j,
            Err(err) => {
                println!("{}", res.text()?);
                panic!("Failed to deserialize from json:\n{:?}", err);
            }
        };

        Ok(jobs)
    }
}

impl Job {
    pub fn get_trace(&self, token: &str, pipeline: &Pipeline) -> Result<String, reqwest::Error> {
        let req_url = format!(
            "https://{repo_url}/api/v4/projects/{project_id}/jobs/{job_id}/trace",
            repo_url = pipeline.repo_url,
            project_id = pipeline.project_id,
            job_id = self.id
        );

        let mut res = reqwest::Client::new()
            .get(&req_url)
            .header("PRIVATE-TOKEN", token)
            .send()?;

        if !res.status().is_success() {
            println!("{}", res.text()?);
            panic!("Response Failed!");
        }

        let trace = format!("-- Trace for: {} --\n{}", self.name, res.text()?);

        Ok(trace)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_url() {
        assert_eq!(
            Pipeline::from_url(
                "https://test.testing.com/namespace1/namespace2/namespace-is-three/pipelines/1234"
            ),
            Pipeline {
                project_id: String::from("namespace1%2Fnamespace2%2Fnamespace-is-three"),
                pipeline_id: 1234.to_string(),
                repo_url: String::from("test.testing.com"),
            }
        );
    }

    #[test]
    #[should_panic]
    fn test_panic_cases() {
        Pipeline::from_url(
            "ttps://test.testing.com/namespace1/namespace2/namespace-is-three/pipelines/1234",
        );
        Pipeline::from_url("https://test.testing.com/pipelines/1234");
        Pipeline::from_url(
            "https://test.testing.com/namespace1/namespace2/namespace-is-three/pipelines/",
        );
    }
}
