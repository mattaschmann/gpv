#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;

extern crate config;
extern crate dirs;
extern crate regex;
extern crate reqwest;
extern crate termion;

use config::{Config, File};
use pipeline::Pipeline;
use std::env::args;
use std::path::Path;
use std::thread;
use std::time::{Duration, Instant};
use termion::{clear, color, cursor};

mod pipeline;

// @Matt TODO: add a way to init config if it does not exist
lazy_static! {
  static ref PRIVATE_TOKEN: String = {
    let mut settings = Config::default();
    let homedir = dirs::home_dir().unwrap().to_str().unwrap().to_owned();
    settings
      .merge(File::from(Path::new(&(homedir + "/.gpv.yaml"))))
      .unwrap();

    settings.get_str("private-token").unwrap().to_string()
  };
}

// @Matt TODO: use the args crate? https://docs.rs/args/2.2.0/args/
fn main() {
  // @Matt TODO: validate args
  let args: Vec<String> = args().collect();
  let pipeline = Pipeline::from_url(&args[1]);
  let start_time = Instant::now();

  let mut trace_len = 0;
  // @Matt TODO: how to init better?
  let mut cur_job = String::from(" ");

  while !check_pipeline(&pipeline, &start_time, &mut trace_len, &mut cur_job) {
    thread::sleep(Duration::from_secs(3));
  }
}

fn check_pipeline(
  pipeline: &Pipeline,
  start_time: &Instant,
  trace_len: &mut usize,
  cur_job: &mut String,
) -> bool {
  let jobs = pipeline.get_jobs(&PRIVATE_TOKEN).unwrap();

  let mut status = Vec::new();
  let mut finished = true;
  let mut trace = String::new();
  let mut this_job = String::new();

  status.push("".to_string());
  status.push(format!("Pipeline: {}", pipeline.web_url));

  for job in jobs {
    let colorize =
      |s: &str, color: String| format!("{}{: <10}{}", color, s, color::Fg(color::Reset));

    let status_str = match job.status.as_ref() {
      "failed" => colorize(&job.status, color::Fg(color::Red).to_string()),
      "success" => colorize(&job.status, color::Fg(color::Green).to_string()),
      "running" => colorize(&job.status, color::Fg(color::Blue).to_string()),
      "pending" => colorize(&job.status, color::Fg(color::Yellow).to_string()),
      _ => format!("{}{: <10}", color::Fg(color::Reset), job.status),
    };

    status.push(format!(
      "{}{} {}",
      status_str,
      color::Fg(color::Reset),
      job.name
    ));

    if finished && ["failed", "running"].contains(&job.status.as_str()) {
      trace = job.get_trace(&PRIVATE_TOKEN, &pipeline).unwrap();
      this_job = job.name;
    }

    if ["running", "pending"].contains(&job.status.as_str()) {
      finished = false;
    }
  }

  let mut screen_buf: Vec<String> = Vec::new();

  let mut trace_buf: Vec<&str> = trace.split("\n").collect();
  trace_buf.pop();
  trace_buf
    .iter()
    .for_each(|s| screen_buf.push(s.to_string()));

  status.iter().for_each(|s| screen_buf.push(s.to_string()));

  screen_buf.push("".to_string());
  screen_buf.push(format!(
    "Seconds Elapsed: {}",
    start_time.elapsed().as_secs()
  ));

  let mut skip = 0;
  if this_job == *cur_job {
    print!(
      "{}{}",
      cursor::Up(2 + status.len() as u16),
      clear::AfterCursor
    );

    skip = *trace_len;
  }

  // @Matt TODO: get the last message from a successful trace
  // @Matt TODO: figure out the new job case
  // @Matt TODO: current figure out the finished case

  screen_buf.iter().skip(skip).for_each(|s| println!("{}", s));

  *trace_len = trace_buf.len();
  *cur_job = this_job;

  finished
}
